<?php
class Login_model
{
    private $table = 'users';

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function checkLogin($data)
    {
        $query = "SELECT * FROM  $this->table WHERE email = :email";
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $user = $this->db->single();
        $passUserDb = $user['password'];
        $passUserForm = $data['password'];

        if (password_verify($passUserForm, $passUserDb)) {
            return $user;
        } else {
            return null;
        }
    }
}
