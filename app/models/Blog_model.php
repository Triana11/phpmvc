<?php
class Blog_model
{
    private $host = DB_HOST;
    private $user = DB_USER;
    private $password = DB_PASSWORD;
    private $db_name = DB_NAME;
    private $db;
    private $table = 'blog';
    private $stmt;
    private $blog = [
        ["penulis" => "Linux", "judul" => "Belajar bla bla bla", "tulisan" => " "],
    ];
    public function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->db_name;
        try {
            $this->db = new PDO($dsn, "root", "");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getAllBlog()
    {
        $this->stmt = $this->db->prepare("SELECT * FROM $this->table");
        $this->stmt->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function create(){
        $query = "INSERT INTO $this->table (penulis, judul, tulisan) VALUES (:penulis, :judul, :tulisan)";
        $this->db->query($query);
        $this->db->bind('penulis', $data['penulis']);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);

        $this->db->execute();
        return $this->db->rowCount();
    }
}
