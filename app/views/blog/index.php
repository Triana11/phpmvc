<div class="container mt-5">
  <div class="row">
    <div class="col-6">
    <a type="button" data-bs-toggle="modal" onclick="" data-bs-target="#addModal" class="btn btn-primary" href="<?=BASE_URL?>/blog/add">Add Blog</a>
      <h3>Blog</h3>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Penulis</th>
            <th scope="col">Judul</th>
            <th scope="col">Tulisan</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody class="table-group-divider">
        <?php foreach ($data["blog"] as $blog): ?>
          <?php $i = 1;?>
          <tr>
            <td><?=$i++;?></td>
            <td><?=$blog["penulis"];?></td>
            <td><?=$blog["judul"];?></td>
            <td><?=$blog["tulisan"];?></td>
            <td>
              <a type="button" data-bs-toggle="modal" data-bs-target="#updateModal" class="btn btn-warning" onclick="return confirm('are you sure want to update?')" href="<?=BASE_URL?>/blog/update/<?=$blog['id']?>">update</a>
              <a type="button" data-bs-toggle="modal" data-bs-target="#deleteModal" class="btn btn-danger" onclick="return confirm('are you sure want to delete?')" href="<?=BASE_URL?>/blog/delete/<?=$blog['id']?>">update</a>
            </td>
          </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    </div>
  </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="addModel" tabindex="-1" aria-labelledby="addModelLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="addModelLabel">Add Blog</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="" method="POST" id="addBlog">
            <div class="mb-3">
              <label for="penulis" class="form-label text-gray-900 font-weight-bold">Penulis</label>
              <input type="text" name="penulis" id="penulis" class="form-control"  placeholder="Input penulis ...">
            </div>
            <div class="mb-3">
              <label for="judul" class="form-label text-gray-900 font-weight-bold">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control" placeholder="Input judul ...">
            </div>
            <div class="mb-3">
              <label for="tulisan" class="form-label text-gray-900 font-weight-bold">Tulisan</label>
              <input type="text" name="tulisan" id="tulisan" class="form-control" placeholder="Input judul ...">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
      </from>
    </div>
  </div>
</div>

<!-- update Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="updateModalLabel">Update Blog</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="" method="POST" id="updateBlog">
            <div class="mb-3">
              <label for="penulis" class="form-label text-gray-900 font-weight-bold">Penulis</label>
              <input type="text" name="penulis" id="penulis" class="form-control" placeholder="Input penulis ...">
            </div>
            <div class="mb-3">
              <label for="judul" class="form-label text-gray-900 font-weight-bold">Judul</label>
              <input type="text" name="judul" id="judul" class="form-control" placeholder="Input judul ...">
            </div>
            <div class="mb-3">
              <label for="tulisan" class="form-label text-gray-900 font-weight-bold">Tulisan</label>
              <input type="text" name="tulisan" id="tulisan" class="form-control" placeholder="Input judul ...">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
