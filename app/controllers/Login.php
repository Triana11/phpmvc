<?php
class Login extends Controller
{
    public function index()
    {
        $this->view("auth/login");
    }

    public function controlerLogin()
    {
        $loginModel = $this->model('Login_model');
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $data = [
                'email' => $_POST['email'],
                'password' => $_POST['password'],
            ];
            $row = $loginModel->checkLogin($data);
            //session
            if ($row) {
                $_SESSION['email'] = $row['email'];
                $_SESSION['password'] = $row['password'];
                $_SESSION['session_login'] = 'login';
                header('location:' . BASE_URL . '/home');
            } else {
                header('location:' . BASE_URL . '/login');
                exit;
            }
        }
    }
}
