<?php
class Home extends Controller
{
    public function __construct()
    {
        if ($_SESSION['session_login'] != 'login') {
            header('location: ' . BASE_URL . '/login');
            exit;
        }
    }
    public function index()
    {
        $data["judul"] = "Home";
        $data["username"] = $this->model("User_model")->getUser();
        $this->view("templates/header", $data);
        $this->view("home/index", $data);
        $this->view("templates/footer");
    }
}
