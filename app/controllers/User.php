<?php
class User extends Controller
{
    public function index()
    {
        $data["judul"] = "User";
        $this->view("templates/header", $data);
        $this->view("user/index");
        $this->view("templates/footer");
    }
    public function profile($name = "Nevelia", $job = "Devs")
    {
        $data["judul"] = "User";
        $data["name"] = $name;
        $data["job"] = $job;
        $this->view("templates/header", $data);
        $this->view("user/profile", $data);
        $this->view("templates/footer");
    }
}
